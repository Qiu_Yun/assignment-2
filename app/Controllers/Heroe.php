<?php

namespace App\Controllers;

class Heroe extends BaseController {

    public function getIndex() {
        // connect to the model
        $places = new \App\Models\Places();
        // retrieve all the records
        $records = $places->findAll();

        $table = new \CodeIgniter\View\Table();

        $headings = $places->fields;
        $displayHeadings = array_slice($headings, 1, 2);
        $table->setHeading(array_map('ucfirst', $displayHeadings));
        $table->setHeading('name','detail','image');
        foreach ($records as $record) {
            $nameLink = anchor("heroe/showme/$record->id", $record->name);
            $table->addRow($nameLink, $record->description,"<img src='/image/$record->image'/>");
        }

        //return $table->generate();



        $template = [
            'table_open' => '<table cellpadding="5px">',
            'cell_start' => '<td style="border: 1px solid #dddddd;">',
            'row_alt_start' => '<tr style="background-color:#dddddd">',
        ];
        $table->setTemplate($template);

        $fields = [
            'title' => 'anti-Japanese hero',
            'heading' => 'anti-Japanese hero',
            'footer' => 'Copyright xie hanwei'
        ];

        $parser = \Config\Services::parser();

        return $parser->setData($fields)
                        ->render('templates\top') .
                $table->generate() .
                        $parser->setData($fields)
                        ->render('templates\bottom');

        // get a template parser
        // tell it about the substitions
//        return $parser->setData(['records' => $records])
//                        // and have it render the template with those
//                        ->render('placeslist');
    }

    public function getShowme($id) {
        // connect to the model
        $places = new \App\Models\Places();
        // retrieve all the records
        $record = $places->find($id);
        
        $table = new \CodeIgniter\View\Table();
        
        $headings = $places->fields;
        $displayHeadings = array_slice($headings,1,2);
        $table->setHeading(array_map('ucfirst', $displayHeadings));
        
        $table->addRow('Name',$record['name']);
        $table->addRow('City',$record['city']);
        $table->addRow('Death time',$record['deathTime']);
        $table->addRow('age',$record['age']);
        $table->addRow('Description',$record['description']);
        $table->addRow('Link',$record['link']);
        $table->addRow('Image',"<img src='/image/$record[image]'/>");
        $table->addRow("<a href='/'>Back</a>");
        
        $template = [
            'table_open' => '<table cellpadding="5px">',
            'cell_start' => '<td style="border: 1px solid #dddddd;">',
            'row_alt_start' => '<tr style="background-color:#dddddd">',
        ];
        $table->setTemplate($template);

        $fields = [
            'title' => 'anti-Japanese hero',
            'heading' => 'anti-Japanese hero',
            'footer' => 'Copyright xie hanwei'
        ];
        $parser = \Config\Services::parser();

        return $parser->setData($fields)
                        ->render('templates\top') .
                $table->generate() .
                        $parser->setData($fields)
                        ->render('templates\bottom');
        // get a template parser
//        $parser = \Config\Services::parser();
        // tell it about the substitions
//        return $parser->setData($record)
//                        // and have it render the template with those
//                        ->render('oneplace');
    }

}
